<?php

namespace App\Http\Controllers\ApiControllers\Company;

use App\Http\Controllers\Controller;
use App\Http\Requests\Company\StoreCompanyLogoRequest;

use App\Http\Resources\CompanyLogoResource;
use App\Models\CompanyLogo;
use Illuminate\Support\Facades\Storage;

class CompanyImagesController extends Controller
{
    public function store(StoreCompanyLogoRequest $request): CompanyLogoResource
    {
        $path = Storage::disk('public')->put('/companiesLogos', $request['logo']);

      $logo = CompanyLogo::create([
            'path' => $path,
            'company_id' => 1,
        ]);

       return new CompanyLogoResource($logo);
    }
}
