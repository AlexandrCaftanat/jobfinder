<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->id();
            $table->string('job_title');
            $table->string('company_name')->nullable();
            $table->unsignedInteger('city');
            $table->unsignedInteger('exact_salary')->nullable();
            $table->unsignedInteger('min_salary')->nullable();
            $table->unsignedInteger('max_salary')->nullable();
            $table->unsignedBigInteger('currency_id')->default(1);
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('schedule_id');
            $table->text('job_description');
            $table->unsignedBigInteger('education_id');
            $table->unsignedBigInteger('work_place_id');
            $table->unsignedBigInteger('work_experience_id');
            $table->boolean('disabled')->nullable();
            $table->boolean('employ_refugees')->nullable();
            $table->boolean('online_interview')->nullable();
            $table->boolean('remote')->nullable();
            $table->unsignedBigInteger('company_id')->nullable();
            $table->unsignedBigInteger('job_contact_id')->nullable();
            $table->dateTime('expire_date');
            $table->dateTime('publication_date');
            $table->timestamps();


            $table->foreign('currency_id')->references('id')->on('currency')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('job_categories')->onDelete('cascade');
            $table->foreign('schedule_id')->references('id')->on('work_schedule')->onDelete('cascade');
            $table->foreign('education_id')->references('id')->on('educations')->onDelete('cascade');
            $table->foreign('work_place_id')->references('id')->on('work_place')->onDelete('cascade');
            $table->foreign('work_experience_id')->references('id')->on('work_experience')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->foreign('job_contact_id')->references('id')->on('job_contacts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('jobs');
    }
};
