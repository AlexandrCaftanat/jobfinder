<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('company_photos', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('company_id');

            $table->string('cover')->nullable();
            $table->string('logo')->nullable();
            $table->string('alt_cover')->nullable();
            $table->string('alt_logo')->nullable();
            $table->boolean('logo_status')->default('false');
            $table->boolean('cover_status')->default('false');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('company_photos');
    }
};
