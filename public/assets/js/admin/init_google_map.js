document.addEventListener('DOMContentLoaded', function() {
    initMap(); // Initialize map as soon as the document is ready
});

let map, jobFlagIcon, jobFlagMarker, marker;

function initMap() {
    map = new google.maps.Map(document.getElementById("map"), {
        center: new google.maps.LatLng(44.4289, 26.0946),
        zoom: 13,
        disableDefaultUI: true,
    });

    jobFlagIcon = {
        url: "../img/angajat-map-marker.png",
        scaledSize: new google.maps.Size(40, 55),
    };

    jobFlagMarker = new google.maps.Marker({
        map: map,
        position: new google.maps.LatLng(44.4289, 26.0946),
        title: "Angajat.ro",
        icon: jobFlagIcon,
    });
}

    // Функция инициализации карты
    function initMap() {
    map = new google.maps.Map(document.getElementById("map"), {
        center: new google.maps.LatLng(44.4289, 26.0946),
        zoom: 13,
        disableDefaultUI: true,
    });

    // Custom icon for the marker
    jobFlagIcon = {
    url: "../img/angajat-map-marker.png", // URL of the custom marker image
    scaledSize: new google.maps.Size(40, 55), // Size of the icon
};

    // Creating a marker with a custom icon
    jobFlagMarker = new google.maps.Marker({
    map: map,
    position: new google.maps.LatLng(44.4289, 26.0946),
    title: "Angajat.ro",
    icon: jobFlagIcon,
});
}

    // Получение элементов
    var modal = document.getElementById("myModal");
    var btn = document.getElementById("showMap");
    var span = document.getElementsByClassName("close")[0];

    // Открыть модальное окно при клике на кнопку
    btn.addEventListener("click", function () {
    modal.style.display = "block";
    // Вызов initMap после открытия модального окна для корректной загрузки карты
    initMap();
});

    document
    .getElementById("address-string")
    .addEventListener("change", function () {
    geocodeAddress(this.value);
});

    function geocodeAddress(address) {
    const geocoder = new google.maps.Geocoder();
    geocoder.geocode({address: address}, function (results, status) {
    if (status === "OK") {
    if (marker) marker.setMap(null); // Clear the previous marker, if any

    map.setCenter(results[0].geometry.location);
    map.setZoom(17);
    marker = new google.maps.Marker({
    map: map,
    position: results[0].geometry.location,
    icon: jobFlagIcon,
});
} else {
    alert(
    "Geocode was not successful for the following reason: " + status
    );
}
});
}

    // Закрыть модальное окно при клике на крестик
    span.onclick = function () {
    modal.style.display = "none";
};
