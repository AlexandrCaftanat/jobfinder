<!-- Модальное окно -->
<div class="modal" id="myModal">
    <div class="modal-dialog modal-xl modal-fullscreen-lg-down">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <h4 class="modal-title">Модальное окно</h4>
                 <span id="showOnMApNotif" style="font-size: 18px;  font-weight: bold; display: none" class="text-success">Apăsați <span style="color: red">Click</span> sau <span style="color: red">Enter</span> pentru a vizualiza pe hartă!</span>
                <button type="button" class="close" data-dismiss="modal">
                    &times;
                </button>
            </div>


            <!-- Основное содержимое модального окна -->
            <div class="modal-body">
                <div class="form-group row">
                    <div class="card ">
                        <div class="form-group row  mt-4">
                            <div class="col-lg-6">
                                <label for="city">{{ __('Oraș:') }}</label>
                                <select class="form-select form-control" name="city" id="city">
                                    <option value="București">București</option>
                                    <option value="Cluj-Napoca">Cluj-Napoca</option>
                                    <option value="Timișoara">Timișoara</option>
                                    <option value="Iași">Iași</option>
                                    <option value="Constanța">Constanța</option>
                                    <option value="Craiova">Craiova</option>
                                    <option value="Brașov">Brașov</option>
                                    <option value="Galați">Galați</option>
                                    <option value="Ploiești">Ploiești</option>
                                    <option value="Oradea">Oradea</option>
                                    <option value="Brăila">Brăila</option>
                                    <option value="Arad">Arad</option>
                                    <option value="Pitești">Pitești</option>
                                    <option value="Sibiu">Sibiu</option>
                                    <option value="Bacău">Bacău</option>
                                    <option value="Târgu Mureș">Târgu Mureș</option>
                                    <option value="Baia Mare">Baia Mare</option>
                                    <option value="Buzău">Buzău</option>
                                    <option value="Botoșani">Botoșani</option>
                                    <option value="Satu Mare">Satu Mare</option>
                                    <option value="Râmnicu Vâlcea">Râmnicu Vâlcea</option>
                                    <option value="Suceava">Suceava</option>
                                    <option value="Drobeta-Turnu Severin">Drobeta-Turnu Severin</option>
                                    <option value="Târgu Jiu">Târgu Jiu</option>
                                    <option value="Tulcea">Tulcea</option>
                                    <option value="Târgoviște">Târgoviște</option>
                                    <option value="Reșița">Reșița</option>
                                    <option value="Focșani">Focșani</option>
                                    <option value="Piatra Neamț">Piatra Neamț</option>
                                    <option value="Alba Iulia">Alba Iulia</option>
                                    <option value="Deva">Deva</option>
                                    <option value="Zalău">Zalău</option>
                                    <option value="Slatina">Slatina</option>
                                    <option value="Călărași">Călărași</option>
                                    <option value="Giurgiu">Giurgiu</option>
                                    <option value="Hunedoara">Hunedoara</option>
                                    <option value="Roman">Roman</option>
                                    <option value="Bârlad">Bârlad</option>
                                    <option value="Sfântu Gheorghe">Sfântu Gheorghe</option>
                                    <option value="Bistrița">Bistrița</option>
                                    <option value="Slobozia">Slobozia</option>
                                    <option value="Vaslui">Vaslui</option>
                                    <option value="Urziceni">Urziceni</option>
                                    <option value="Mediaș">Mediaș</option>
                                    <option value="Vulcan">Vulcan</option>
                                </select>
                            </div>
                            <div class="col-lg-6">


                                <label for="address-string">{{ __('Adresa:') }}</label>
                                <input id="address-string" type="text" name="address-string"
                                       class="form-control"
                                       placeholder="{{ __('introduceti adresa') }}" required/>



                            </div>
                        </div>

                    </div>
                </div>

                <div
                    id="map"
                    style="height: 500px; max-width: 100%; background-color: bisque"
                ></div>
            </div>
            <!-- Кнопка для закрытия модального окна -->
            <div class="modal-footer">
                <button type="button" data-bs-dismiss="modal" class="btn btn-secondary btn-padding">Anulați</button>
                <button type="submit" id="apply-address" class="btn btn-danger btn-padding px-5">Salvați</button>

            </div>
        </div>
    </div>
</div>

