<style>
    .required:after {
        content: '*';
        color: red;
        margin-left: 3px;
    }

    .form-control {
        font-size: 1.2rem;
    }

    input.show-phone {
        width: 1.3rem;
        height: 1.3rem;
    }


    .form-select {
        --bs-form-select-bg-img: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3e%3cpath fill='none' stroke='%23343a40' stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='m2 5 6 6 6-6'/%3e%3c/svg%3e");
        display: block;
        width: 100%;
        padding: .375rem 2.25rem .375rem .75rem;
        font-size: 1.2rem;
        font-weight: 400;
        line-height: 1.5;
        color: var(--bs-body-color);
        background-color: var(--bs-body-bg);
        background-image: var(--bs-form-select-bg-img), var(--bs-form-select-bg-icon, none);
        background-repeat: no-repeat;
        background-position: right .75rem center;
        background-size: 12px 8px;
        transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none
    }


    .form-group label {
        font-size: 15px;
        font-weight: lighter;
    }

    .phone-btn {
        /*font-style: ;*/
        padding: 8px 8px;
    }

    #city {
        font-size: 16px;

    }

    #address-string {
        font-size: 16px;
    }


    .address-info {
        margin-bottom: 10px;
    }


    .gap-3 {
        gap: 1rem !important;
    }

    .upload-images .img {
        width: 113px;
        height: 90px;
        border-radius: 4px;
        background-size: cover;
        position: relative;
        background-position: center;
    }


    /*.img {*/
    /*    position: relative;*/
    /*    display: inline-block;*/
    /*}*/

    .img a {
        position: absolute;
        top: 0;
        right: 0;
        margin: 3px;
        background-color: rgba(255, 255, 255, 0.8); /* Цвет фона иконки (прозрачный белый) */
        padding: 2px 5px; /* Отступ вокруг иконки */
        border-radius: 20%; /* Скругление углов для создания круглой формы */
    }

    .img a i {
        color: #BF362F;
    }

    input[type="file"] {
        display: none;
    }




    .btn {
        color: #42b983;
        position: relative;
        border-radius: 12px;
        border: 1px solid #42b983;
        text-decoration: none;
        text-transform: uppercase;
        padding: 0.5rem 1.5rem;
        font-weight: 700;
        outline: none;
        background-color: #fff;
        transition: all 0.22s;
        cursor: pointer;
        margin-right: 1rem;
    }

    .btn.primary {
        background-color: #42b983;
        color: #fff;
    }

    .btn:active {
        box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.3);
    }

    .btn:hover {
        cursor: pointer;
        opacity: 0.8;
    }

    input[type="file"] {
        display: none;
    }

    .preview {
        display: flex;
        flex-wrap: wrap;
        padding-top: 0.5rem;
    }

    .preview-image {
        position: relative;
        margin-bottom: 0.5rem;
        margin-right: 0.5rem;
        overflow: hidden;
    }

    .preview-image.removing {
        transform: scale(0);
        transition: transform 0.3s;
    }
    .preview-image img {
        width: 180px;
        height: auto;
    }



    .preview-remove {
        opacity: 0;
        width: 20px;
        height: 20px;
        position: absolute;
        right: 0;
        top: 0;
        font-weight: bold;
        background-color: rgba(255, 4, 255, 0.5);
        display: flex;
        align-content: center;
        justify-content: center;
        cursor: pointer;
        transition: opacity 0.22s;
    }

    .preview-image:hover .preview-remove {
        opacity: 1;
    }

    /*.upload-images .img a {*/
    /*    display: block;*/
    /*    background-color: #F2D2D0;*/
    /*    padding: 2px 4px;*/
    /*    font-size: 18px;*/
    /*    position: absolute;*/
    /*    top: 8px;*/
    /*    right: 8px;*/
    /*    border-radius: 3px;*/
    /*}*/
</style>


<div class="flex-row-fluid ml-lg-9 b-card">
    <!--begin::Card-->
    <div class="card card-custom card-stretch">
        <!--begin::Header-->
        <div class="card-header py-3">
            <div class="card-title align-items-start flex-column">
                <h3 class="card-label font-weight-bolder text-dark">Personal Information</h3>
                <span class="text-muted font-weight-bold font-size-sm mt-1">Update your personal information</span>
            </div>
        </div>
        <!--end::Header-->

        <!--begin::Form-->
        <form class="form">
            <!--begin::Body-->
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-9 col-xl-6 text-left">
                        <h5 class="font-weight-bold mb-6 text-uppercase">{{ __('logoul companiei') }}</h5>
                    </div>
                </div>

                @include('includes.admin.company-logo-block')


                <div class="row">
                    <div class="col-lg-9 col-xl-6 text-left">
                        <h5 class="font-weight-bold mb-6 text-uppercase">{{ __('Informații despre companie') }}</h5>
                    </div>
                </div>

                <!-- Personal Information Fields Here -->
                <div class="form-group row">
                    <div class="card">
                        <div class="form-group row mt-5">
                            <div class="col-lg-6">
                                <label class="required">{{ __('Tip cont:') }}</label>
                                <select name="fizica_juridica" class="form-control form-select">
                                    <option value="0"> Persoana Juridică</option>
                                    <option value="1"> Persoana Fizică</option>
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <label class="required">{{ __('Denumirea companiei:') }}</label>
                                <input type="text" name="company_name" class="form-control"
                                       placeholder="{{ __('Denumirea companiei:') }}"/>
                            </div>
                        </div>


                        <div class="company-contacts">
                            <div class="form-group row mt-5">
                                <div class="col-lg-6">
                                    <label for="legal_name">{{ __('Denumire juridică:') }}</label>
                                    <input type="text" name="legal_name" class="form-control"
                                           id="legal_name" placeholder="{{ __('Denumire juridică') }}"/>

                                </div>

                                <div class="col-lg-6">
                                    <label>Domeniul de activitate:</label>
                                    <select name="activity" class="form-select form-control">
                                        <option selected="" value="0">Alege domeniul</option>
                                        <option value="539">Administrație / Sector Public</option>
                                        <option value="857">Arhitectură / Design Interior</option>
                                        <option value="540">Alimentar</option>
                                        <option value="542">Artă / Entertainment</option>
                                        <option value="538">Agricultură/ Silvicultură / Pescuit</option>
                                        <option value="543">Construcții</option>
                                        <option value="544">Asigurare</option>
                                        <option value="545">Call-center / BPO</option>
                                        <option value="547">Comerț / Retail</option>
                                        <option value="548">Bănci / Servicii financiare</option>
                                        <option value="549">Imobiliare</option>
                                        <option value="550">Educație / Training</option>
                                        <option value="551">Energetică</option>
                                        <option value="552">Farma</option>
                                        <option value="553">Învățământ</option>
                                        <option value="554">Sănătate și asistență socială</option>
                                        <option value="555">Activități de spectacole, culturale și recreative</option>
                                        <option value="556">IT / Telecom</option>
                                        <option value="558">Lemn / PVC</option>
                                        <option value="567">Resurse Umane</option>
                                        <option value="568">Mașini / Auto</option>
                                        <option value="569">Media / Internet</option>
                                        <option value="570">Medicină / Sănătate</option>
                                        <option value="571">Naval / Aeronautic</option>
                                        <option value="572">Pază și protecție</option>
                                        <option value="574">Petrol / Gaze</option>
                                        <option value="575">Prestări servicii</option>
                                        <option value="576">Protecția mediului</option>
                                        <option value="577">Publicitate / Marketing / PR</option>
                                        <option value="578">Sport / Frumusețe</option>
                                        <option value="579">Textilă</option>
                                        <option value="580">Transport / Logistică / Import - Export</option>
                                        <option value="581">Turism / HoReCa</option>
                                        <option value="583">Producere</option>
                                        <option value="608">Divertisment</option>
                                        <option value="689">ONG</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="form-group row">
                    <div class="card ">
                        <div class="mb-3 flex-grow-1">
                            <div class="row">
                                <div class="col-6 mb-1 mt-5">
                                    <label class="form-label">{{ __('Număr de contact al companiei:') }}</label>
                                </div>
                            </div>

                            @include('includes.admin.company-phones')
                        </div>

                        <div class="separator separator-dashed mb-3"></div>

                        <div class="mb-5 flex-grow-1">
                            <div class="row">
                                <div class="col-6 mb-1">
                                    <label class="form-label required">{{ __('Email pentru notificări:') }}</label>
                                </div>
                            </div>

                            @include('includes.admin.company-email')

                        </div>
                    </div>
                </div>


                <div class="form-group row">
                    <div class="card ">
                        <div class="form-group row mt-5">
                            <div class="col-lg-6">
                                <label for="company_site">{{ __('Pagina web:') }}</label>
                                <input id="company_site" type="text" name="company_site" class="form-control"
                                       placeholder="{{ __('Pagina web') }}"/>
                            </div>
                            <div class="col-lg-6">
                                <label for="company_number_of_employer">{{ __('Nr. angajaților în companie:') }}</label>
                                <input id="company_number_of_employer" type="text" name="company_number_of_employer"
                                       class="form-control"
                                       placeholder="{{ __('Numărul angajaților în cadrul companiei') }}"/>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-group row">
                    <div class="address-info">
                        <div class="row">
                            <div class="col-lg-9 text-left">
                                <h5 class="font-weight-bold mt-2 text-uppercase">{{__('Adresa Companiei:')}}</h5>
                            </div>
                            <div class="col-lg-2 d-flex justify-content-start">
                                <button style="font-size: 16px;  font-family: 'Roboto Slab';"
                                        type="button"
                                        class="btn btn-outline-dark"
                                        data-toggle="modal"
                                        data-target="#myModal"
                                        id="showMap"
                                >
                                    Adaugă
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="separator separator-dashed mb-5"></div>


                <div class="form-group row">
                    <div class="address-info">
                        <div class="row">
                            <div class="col-lg-9 text-left">
                                <h5 class="font-weight-bold mt-2 text-uppercase">{{__('Beneficiile oferite de
                                    companie:')}}</h5>
                            </div>
                            <div class="col-lg-2 d-flex justify-content-start">
                                <button style="font-size: 16px;  font-family: 'Roboto Slab';"
                                        type="button"
                                        class="btn btn-outline-dark"
                                        data-toggle="modal"
                                        data-target="#benefitsModal"
                                >
                                    Adaugă
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="separator separator-dashed mb-5"></div>

<!--                                <div class="form-group row">-->
<!--                                    <div class="address-info">-->
<!--                                        <div class="row">-->
<!--                                            <div class="col-lg-10 text-left">-->
<!--                                                <h5 class="font-weight-bold mt-2 text-uppercase">{{__('GALERIE DE IMAGINI:')}}</h5>-->
<!--                                            </div>-->
<!---->
<!--                                            <div class="col-lg-2 d-flex justify-content-start">-->
<!--                                                <button style="font-size: 16px;  font-family: 'Roboto Slab';"-->
<!--                                                        type="button"-->
<!--                                                        class="btn btn-outline-dark addImage"-->
<!--                                                >-->
<!--                                                    Adaugă-->
<!--                                                </button>-->
<!--                                            </div>-->
<!--                                            <input type="file" class="file-input" accept="image/*" id="file-input" />-->
<!--                                        </div>-->
<!---->
<!--                                    </div>-->
<!--                                </div>-->




                <div class="container">
                    <div class="card">
                        <input type="file" class="file-input" id="file-input" />
<!--                         <button class="btn open" id="open">Открыть</button>-->
                        <!-- <button class="btn primary">Загрузить</button> -->
                    </div>
                </div>




                <div class="form-group row">
                    <div class="card">
                        <div class="row">
                            <div class="col-lg-9 col-xl-6 text-left">
                                <h5 class="font-weight-bold mt-5 text-uppercase">{{__('DESPRE COMPANIE:')}}</h5>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="kt-ckeditor-1" style="height: 325px;">
                                Comunicați candidaților despre companie ...
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-lg-6">
                            <button type="reset" class="btn btn-primary mr-2">Save</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                        <div class="col-lg-6 text-right">
                            <button type="reset" class="btn btn-danger">Delete</button>
                        </div>
                    </div>
                </div>
            </div> <!-- end::Card-->

            <!-- Модальное окно -->
            @include('includes.admin.address-modal')
            <!--end::Body-->
        </form>
        <!--end::Form-->
    </div>
</div>
    <!--end::Card-->

    @include('includes.admin.benefits-modal')

    <script src="https://cdn.ckeditor.com/ckeditor5/35.0.1/classic/ckeditor.js"></script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfkSRftq-dnOSMSso1f05hxK1sUUnD35Q&language=ro&callback=initMap&libraries=places,marker"></script>
    <script src="./assets/js/admin/galllery.js"></script>





    <script>
        let map, jobFlagIcon, jobFlagMarker, companyAddress;
        let modal = document.getElementById("myModal");
        let btn = document.getElementById("showMap");
        let span = document.getElementsByClassName("close")[0];


        const citySelect = document.getElementById('city');
        const applyBtn = document.getElementById('apply-address');
        const addressInput = document.getElementById("address-string")

        if (btn !== null) {
            btn.addEventListener("click", function () {
                modal.style.display = "block";
                google.maps.event.trigger(map, "resize");
                map.setCenter(new google.maps.LatLng(44.4289, 26.0946)); // Re-center map
            });
        }


        span.onclick = function () {
            modal.style.display = "none";
        };


        addressInput.addEventListener("change", function () {
            geocodeAddress(this.value);
        });
        addressInput.addEventListener("input", function () {
            document.getElementById('showOnMApNotif').style.display = 'block';
        });


        document.addEventListener('DOMContentLoaded', () => {
            const address = document.getElementById('address-string');
            address.value = `${citySelect.value}, `;

            citySelect.addEventListener('change', () => {
                address.value = `${citySelect.value}, `;
                geocodeAddress(address.value, true, 13);
            });
        });

        function initMap() {
            map = new google.maps.Map(document.getElementById("map"), {
                center: new google.maps.LatLng(44.4289, 26.0946),
                zoom: 13,
                disableDefaultUI: true,
                gestureHandling: 'greedy',
            });

            jobFlagIcon = {
                url: "{{asset('assets/images/logo/angajat-map-marker.png')}}",
                scaledSize: new google.maps.Size(40, 55),
            };

            jobFlagMarker = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng(44.4289, 26.0946),
                title: "Angajat.ro",
                icon: jobFlagIcon,
            });
        }


        function geocodeAddress(address, cityOnly = false, zoom = 15) {
            const geocoder = new google.maps.Geocoder();
            let geocodeParams = {address: address};

            if (cityOnly) {
                geocodeParams = {address: address, componentRestrictions: {locality: address}};

            }

            geocoder.geocode(geocodeParams, function (results, status) {
                if (status === "OK") {
                    if (results[0] && results[0].geometry && results[0].geometry.location_type === 'ROOFTOP') {
                        // console.log(status);
                        // console.log(results);
                        companyAddress = results[0].formatted_address;
                        // console.log(companyAddress);
                        map.setCenter(results[0].geometry.location);
                        map.setZoom(zoom);
                        jobFlagMarker.setPosition(results[0].geometry.location);
                    } else if (cityOnly) {
                        console.log('City center found, setting map center.');
                        map.setCenter(results[0].geometry.location);
                        map.setZoom(zoom);
                        jobFlagMarker.setPosition(results[0].geometry.location);
                    } else {
                        alert("The address provided is incorrect or too vague.");
                    }
                } else {
                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
        }


        // applyBtn.addEventListener('click', (e) => {
        //     e.preventDefault();
        // }

        // Text Area

        var KTCkeditor = (function () {
            // Private functions
            var demos = function () {
                ClassicEditor
                    .create(document.querySelector('#kt-ckeditor-1'), {
                        toolbar: ['heading', '|', 'bold', 'italic', 'bulletedList', 'numberedList', 'blockQuote',],
                    })
                    .then(editor => {
                        console.log(editor);
                    })
                    .catch(error => {
                        console.error(error);
                    });
            };

            return {
                init: function () {
                    demos();
                }
            };
        })();

        // Initialization
        document.addEventListener('DOMContentLoaded', function () {
            KTCkeditor.init();
        });


    </script>



