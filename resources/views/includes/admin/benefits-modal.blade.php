<style>
    .form-check-label {
        font-size: 18px;
    }

    .form-check .form-check-input {
        float: left;
        margin-left: -2.0em;
        margin-right: -2.5em;
    }

    .form-check-input:checked[type=checkbox] {
        background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'%3e%3cpath fill='none' stroke='%23FF0000' stroke-linecap='butt' stroke-linejoin='butt' stroke-width='3' d='m6 10 3 3 6-6'/%3e%3c/svg%3e");
        background-size: 22px 22px;
    }


    .form-check-input:checked {
        background-color: #ffffff;
        border-color: #FF0000;
    }
    .form-check-input:hover {
        border-color: #FF0000;
        background-color: #F0F0F0;
        box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.5);
    }


    .form-check-input {

        width: 1.4em;
        height: 1.4em;
        margin-top: .25em;
        vertical-align: top;
        background-color: #fff;
        background-repeat: no-repeat;
        background-position: center;
        background-size: contain;
        border: 1px solid #adb5bd;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        -webkit-print-color-adjust: exact;
    }


</style>
<!-- The Modal -->
<div class="modal" id="benefitsModal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Modal Heading</h4>
                <button type="button" class="close" data-dismiss="modal">
                    &times;
                </button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form>
                    <div class="mb-5 row">
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="5"
                                    id="cat_5"
                                    data-gtm-form-interact-field-id="2"
                                />
                                <label class="form-check-label" for="cat_5">
                                    Reduceri la produsele / serviciile companiei.
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="3"
                                    id="cat_3"
                                />
                                <label class="form-check-label" for="cat_3">
                                    Mobil
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="4"
                                    id="cat_4"
                                />
                                <label class="form-check-label" for="cat_4">
                                    Remote job
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="6"
                                    id="cat_6"
                                />
                                <label class="form-check-label" for="cat_6">
                                    Mașină de serviciu / Decontare transport
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="7"
                                    id="cat_7"
                                />
                                <label class="form-check-label" for="cat_7">
                                    Fructe proaspete
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="38"
                                    id="cat_38"
                                />
                                <label class="form-check-label" for="cat_38">
                                    Recompensarea loialității
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="8"
                                    id="cat_8"
                                />
                                <label class="form-check-label" for="cat_8">
                                    Ceai / cafea / snacksuri
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="9"
                                    id="cat_9"
                                />
                                <label class="form-check-label" for="cat_9">
                                    Acces la biblioteca companiei și alte resurse
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="10"
                                    id="cat_10"
                                />
                                <label class="form-check-label" for="cat_10">
                                    Tichete de masă
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="12"
                                    id="cat_12"
                                />
                                <label class="form-check-label" for="cat_12">
                                    Zonă de recreere
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="13"
                                    id="cat_13"
                                />
                                <label class="form-check-label" for="cat_13">
                                    Abonament sală de sport
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="15"
                                    id="cat_15"
                                />
                                <label class="form-check-label" for="cat_15">
                                    Cărți de vizită
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="16"
                                    id="cat_16"
                                />
                                <label class="form-check-label" for="cat_16">
                                    Deplasări de serviciu
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="17"
                                    id="cat_17"
                                />
                                <label class="form-check-label" for="cat_17">
                                    Bonusuri și prime în funcție de performanță
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="18"
                                    id="cat_18"
                                />
                                <label class="form-check-label" for="cat_18">
                                    Team - building
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="19"
                                    id="cat_19"
                                />
                                <label class="form-check-label" for="cat_19">
                                    Cursuri de limbi străine
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="20"
                                    id="cat_20"
                                />
                                <label class="form-check-label" for="cat_20">
                                    Traininguri
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="21"
                                    id="cat_21"
                                />
                                <label class="form-check-label" for="cat_21">
                                    Uniformă
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="22"
                                    id="cat_22"
                                />
                                <label class="form-check-label" for="cat_22">
                                    Asigurare medicală privată
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="24"
                                    id="cat_24"
                                />
                                <label class="form-check-label" for="cat_24">
                                    Program flexibil
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="26"
                                    id="cat_26"
                                />
                                <label class="form-check-label" for="cat_26">
                                    Zile suplimentare de concediu
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="27"
                                    id="cat_27"
                                />
                                <label class="form-check-label" for="cat_27">
                                    Daruri pentru diferite ocazii
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="28"
                                    id="cat_28"
                                />
                                <label class="form-check-label" for="cat_28">
                                    Călătorii
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="29"
                                    id="cat_29"
                                />
                                <label class="form-check-label" for="cat_29">
                                    Acoperire costuri certificări profesionale
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="31"
                                    id="cat_31"
                                />
                                <label class="form-check-label" for="cat_31">
                                    Pachete de wellbeing
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="32"
                                    id="cat_32"
                                />
                                <label class="form-check-label" for="cat_32">
                                    Finanțarea programelor de tip MBA
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="33"
                                    id="cat_33"
                                />
                                <label class="form-check-label" for="cat_33">
                                    Ajutor căsătorie, naștere copil
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="34"
                                    id="cat_34"
                                />
                                <label class="form-check-label" for="cat_34">
                                    Cazare pentru cei care se relochează
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="35"
                                    id="cat_35"
                                />
                                <label class="form-check-label" for="cat_35">
                                    Echilibru viață personală / profesională
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="39"
                                    id="cat_39"
                                />
                                <label class="form-check-label" for="cat_39">
                                    Personalizarea beneficiilor
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="40"
                                    id="cat_40"
                                />
                                <label class="form-check-label" for="cat_40">
                                    Oferirea unor acțiuni la companie
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="41"
                                    id="cat_41"
                                />
                                <label class="form-check-label" for="cat_41">
                                    Vouchere
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="42"
                                    id="cat_42"
                                />
                                <label class="form-check-label" for="cat_42">
                                    Over Time plătit
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-check">
                                <input
                                    class="form-check-input"
                                    name="beneficii[]"
                                    type="checkbox"
                                    value="43"
                                    id="cat_43"
                                />
                                <label class="form-check-label" for="cat_43">
                                    Validarea competențelor prin promovări
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="btn-modal-min">
                        <button type="submit" class="btn btn-danger btn-padding me-3">
                            Salvați
                        </button>
                        <button
                            type="button"
                            class="btn btn-secondary btn-padding"
                            data-bs-dismiss="modal"
                        >
                            Anulați
                        </button>
                    </div>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>
