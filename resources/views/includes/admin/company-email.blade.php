<div class="email-list  mt-3 mb-3">
    <div class="mb-2 email-row row">
            <div class="col-lg-6 col-6">
                <input type="text" class="form-control control-custom phone-mask" name="emails[${index}][email]" value="">
            </div>
            <div class="col-lg-4 col-6">
                <div class="form-check mt-3">
                    <input class="form-check-input show-phone " type="checkbox" name="emails[0][hidden]" value="1" id="em_0">
                    <label class="form-check-label ms-2" for="em_0">
                        Ascunde pe site                                                </label>
                </div>
            </div>
            <div class="col-6 align-right mt-2 mb-2">
                <a class="text-primary addEmailRow" style="font-size: 14px" href="#">Adaugă încă un email <span class="text-success"><i class="fas fa-plus fa-sm"></i></span></a>
            </div>
        </div>
</div>



<script>
    const emailRow = document.querySelector('.email-list'); // помещаем сюда
    const addEmailLink = document.querySelector('.addEmailRow'); //кнопка



    addEmailLink.addEventListener('click', addEmailRow)


    function addEmailRow(event) {
        event.preventDefault();


     let index = emailRow.children.length;

        const newEmailRowHTML = `

                                    <div class="col-lg-6 col-6">
                                        <input type="text" class="form-control control-custom"
                                               name="emails[${index}][email]" value="">
                                    </div>
                                    <div class="col-lg-4 col-6">
                                        <div class="form-check mt-3">
                                            <input class="form-check-input show-phone " type="checkbox"
                                                   name="emails[${index}][hidden]" value="1" id="em_${index}">
                                            <label class="form-check-label ms-2" for="em_${index}">
                                                Ascunde pe site </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-6 pt-lg-3 pt-4 text-end">
                                        <a href="#"
                                           class="btn btn-outline-danger deleteLogo deleteEmailRow">Ștergeți</a>
                                    </div>
                                    <div class="col-6 align-right mt-1">
                                        <a class="text-primary addEmailRow" style="font-size: 14px" href="#">Adaugă încă
                                            un email <span class="text-success"><i class="fas fa-plus fa-sm"></i></span></a>
                                    </div>

    `;

        const newEmailRow = document.createElement('div');
        newEmailRow.classList.add("mb-2", "email-row", "row");
        newEmailRow.innerHTML = newEmailRowHTML;
        emailRow.appendChild(newEmailRow);
        const newAddEmailLink = newEmailRow.querySelector('.addEmailRow');
        newAddEmailLink.addEventListener('click', addEmailRow);

        const newDeleteEmailLink = newEmailRow.querySelector('.deleteEmailRow');

        if (newDeleteEmailLink) {
            newDeleteEmailLink.addEventListener('click', deleteEmailRow);
        }
    }



    function deleteEmailRow(event) {
        event.preventDefault();
        const emailRowToDelete = event.target.closest('.email-row');

        if (emailRowToDelete) {
            emailRowToDelete.remove();
        }
    }
</script>
