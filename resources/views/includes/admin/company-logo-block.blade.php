<style>
    .update-photo {
        margin-top: 40px;
        margin-left: 20px;
        max-width: 220px;
        height: 220px;
        border: solid 1px #0a6aa1;
        display: flex;
        justify-content: center;
        align-items: center;
        overflow: hidden;
    }


    #companyLogo {
        max-width: 100%;
        max-height: 100%;
        object-fit: contain;
    }

  .logoBtn {
        font-size: 17px;
        padding-top: 6px;
        padding-bottom: 6px;
    }

</style>

<div class="form-group row">
    <div class="card mb-3">
        <div class="row g-0">
            <div class="col-md-5 d-flex justify-content-center align-items-center update-photo p-0">
                <img src="{{ asset('assets/images/user/no_image-500x500.png') }}" class="img-fluid" id="companyLogo" alt="Company Logo">
            </div>
            <div class="col-md-7 d-flex justify-content-center align-items-center my-4">
                <div class="card-body logo-rules">
                    <p class="card-text"><small class="text-muted">{{ __('Cerințe pentru imagine:') }}</small></p>
                    <p class="card-text">{{__('Sunt permise numai imagini pătrate. Dimensiunea minimă de 500x500 px.')}}</p>
                    <p class="card-text mb-1"><small class="text-muted">{{ __('Recomandare:') }}</small></p>
                    <p class="card-text">{{__('Pentru ca logo-ul să arate bine în profilul companiei, încadrați-l pe centru, lăsând spații pe margini.')}}</p>
                    <div class="card-toolbar" id="imageControls">
                        <input type="file" hidden class="addCompanyLogo" id="logoInput">
                        <button type="button" class="btn  btn-outline-dark mr-2 logoBtn" id="addImageBtn">{{ __('Adaugă') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

<script>

    const addImageBtn = document.querySelector('#addImageBtn');
    const logoInput = document.querySelector('.addCompanyLogo');
    const companyLogo = document.querySelector('#companyLogo');
    const imageControls = document.querySelector('#imageControls');
    const placeholderLogoSrc = "{{ asset('assets/images/user/no_image-500x500.png') }}";

    addImageBtn.addEventListener('click', function (e) {
        logoInput.click();
    });

    logoInput.addEventListener('change', function (e) {
        const logo = e.target.files[0];
        const formData = new FormData();
        formData.append('logo', logo);

        axios.post('/api/store_company_logo', formData)
            .then(res => {
                const logoUrl = res.data.data.url;
                if (logoUrl) {
                    companyLogo.src = logoUrl;
                    addCancelButton();
                }
            });
    });

    function deleteLogo() {
        companyLogo.src = placeholderLogoSrc;
        imageControls.removeChild(document.querySelector('#deleteLogo'));
    }

    function addCancelButton() {
        if (!document.querySelector('.cancelButton')) {
            const cancelButton = document.createElement('button');
            cancelButton.setAttribute('type', 'reset');
            cancelButton.classList.add('btn', 'btn-outline-danger', 'logoBtn');
            cancelButton.textContent = '{{ __('Şterge') }}';
            cancelButton.id = 'deleteLogo';
            cancelButton.addEventListener('click', deleteLogo);

            imageControls.appendChild(cancelButton);
        }
    }


</script>


