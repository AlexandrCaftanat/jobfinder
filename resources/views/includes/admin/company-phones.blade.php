<div class="phones-list  mt-3 mb-3">
    <div class="mb-2 phone-row row">
        <div class="col-lg-3 col-6">
            <select class="form-control form-select" name="phones[0][code]">
                <option value="+373">Moldova +373</option>
            </select>
        </div>
        <div class="col-lg-3 col-6">
            <input type="text" class="form-control control-custom phone-mask" name="phones[0][phone]" value="">
        </div>
        <div class="col-lg-4 col-6">
            <div class="form-check mt-3">
                <input class="form-check-input show-phone " type="checkbox" name="phones[0][hidden]" value="1" id="hh_0">
                <label class="form-check-label ms-2" for="hh_0">
                    Ascunde pe site                                                </label>
            </div>
        </div>
        <div class="col-6 align-right mt-2 mb-2">
            <a class="text-primary addPhoneRow" style="font-size: 14px" href="#">Adaugă încă un numar <span class="text-success"><i class="fas fa-plus fa-sm"></i></span></a>
        </div>

    </div>
</div>



<script>
    const phoneRow = document.querySelector('.phones-list'); // помещаем сюда
    const addNumberLink = document.querySelector('.addPhoneRow'); //кнопка



    addNumberLink.addEventListener('click', addPhoneRow)


    function addPhoneRow(event) {
        event.preventDefault();

        let index = phoneRow.children.length;

        const newPhoneRowHTML = `
            <div class="col-lg-3 col-6">
                <select class="form-control form-select" name="phones[0][code]">
                    <option value="+373">Moldova +373</option>
                </select>
            </div>
            <div class="col-lg-3 col-6">
            <input type="text" class="form-control control-custom phone-mask" name="phones[${index}][phone]" value="">
        </div>
        <div class="col-lg-4 col-6">
            <div class="form-check mt-3">
                <input class="form-check-input show-phone" type="checkbox" name="phones[${index}][hidden]" value="1" id="hh_${index}">
                <label class="form-check-label ms-2" for="hh_${index}">
                    Ascunde pe site
                </label>
            </div>
        </div>
            <div class="col-lg-2 col-6 pt-lg-3 pt-4 text-end">
                <a href="#" class="btn btn-outline-danger deleteLogo deletePhoneRow">Șterge</a>
            </div>
            <div class="col-6 align-right mt-1">
                <a class="text-primary addPhoneRow" style="font-size: 14px" href="#">Adaugă încă un numar <span class="text-success"><i class="fas fa-plus fa-sm"></i></span></a>
            </div>
    `;

        const newPhoneRow = document.createElement('div');
        newPhoneRow.classList.add("mb-2", "phone-row", "row");
        newPhoneRow.innerHTML = newPhoneRowHTML;
        phoneRow.appendChild(newPhoneRow);
        const newAddNumberLink = newPhoneRow.querySelector('.addPhoneRow');
        newAddNumberLink.addEventListener('click', addPhoneRow);

        const newDeleteNumberLink = newPhoneRow.querySelector('.deletePhoneRow');
        if (newDeleteNumberLink) {
            newDeleteNumberLink.addEventListener('click', deletePhoneRow);
        }
    }



    function deletePhoneRow(event) {
        event.preventDefault();
        const phoneRowToDelete = event.target.closest('.phone-row');

        if (phoneRowToDelete) {
            phoneRowToDelete.remove();
        }
    }
</script>
