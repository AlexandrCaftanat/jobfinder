<!doctype html>
<html lang="en">

@include('includes.header')

<body id="kt_body" class="header-fixed subheader-enabled">

<!--Navbar Start-->
@include('includes.nav')
<!-- Navbar End-->


<!-- START SIGN-UP MODAL -->
@include('includes.login_modal')
<!-- END SIGN-UP MODAL -->


@yield('content')
<!-- end main content-->



@include('includes.footers_and_scripts')
</body>
</html>
