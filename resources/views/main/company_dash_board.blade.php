@extends('layouts.admin')
@section('content')
<!--begin::Container-->
<section class="company-admin-section">
    <div class="d-flex flex-row flex-column-fluid container-xxl">
        <!--begin::Content Wrapper-->
        <div class="main d-flex flex-column flex-row-fluid">
            <!--begin::Subheader-->
@include('includes.admin.subheader')
            <!--end::Subheader-->

            <div class="content flex-column-fluid" id="kt_content">
                <!--begin::Profile Personal Information-->
                <div class="d-flex flex-row">
                    <!--begin::Aside-->
@include('includes.admin.a-side')
                    <!--end::Aside-->
                    <!--begin::Content-->
@include('includes.admin.b-side-form')
                    <!--end::Content-->
                </div>
                <!--end::Profile Personal Information-->
            </div>
            <!--end::Content-->
        </div>
        <!--begin::Content Wrapper-->
    </div>
</section>

<!--end::Container-->


@endsection




