@extends('layouts.main')
@section('content')
<style>
    .cities {
        font-size: 15px;
        font-weight: 300;
    }

    .jspPane {
        overflow-y: auto;
        max-height: 300px;
        width: auto;
        border: 1px solid #ccc;
        padding: 10px;
        box-sizing: border-box;
    }

    .sb_menu {
        list-style-type: none;
        padding: 0;
        margin: 0;
    }

    .sb_menu li {
        padding: 8px;
        border-bottom: 1px solid #eee;
    }

    .sb_menu li:last-child {
        border-bottom: none;
    }

    .sb_menu li a {
        text-decoration: none;
        color: #333;
        display: block;
    }

</style>
<section class="section mt-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="bg-soft-primary p-3">
                    <h1 class=" text-uppercase mb-0 fs-17">Postează un loc de muncă nou</h1>
                </div>
            </div><!--end col-->
        </div><!--end row-->

        <form action="#" class="job-post-form shadow mt-4 needs-validation" novalidate>
            <div class="job-post-content box-shadow-md rounded-3 p-4">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="mb-4">
                            <label  class="form-label" for="function-name">Funcția <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="function-name" placeholder="Funcția" required>
                            <div class="invalid-feedback">
                                Introduceți denumirea funcției
                            </div>
                        </div>

                        <a href="#" class="btn btn-success hover-rotate-end">Rotate to end</a>
                    </div><!--end col-->
                    <div class="col-lg-12">
                        <div class="mb-4">
                            <label  class="form-label" for="company-name">Numele companiei</label>
                            <input type="text" class="form-control" id="company-name" placeholder="Compania">
                            <div class="invalid-feedback">
                                Introduceți numele companiei
                            </div>
                        </div>
                    </div><!--end col-->
                    <div class="col-lg-12">
                        <div class="mb-4">
                            <label for="choices-single-cities" class="form-label">Oraș<span class="text-danger">*</span></label>
                            <select class="form-select cities" data-trigger="" name="choices-single-cities" id="choices-single-cities" aria-label="Alege oraș" required>
                                <option selected disabled value="">Alege oraș</option>
                                <option value="bu">Bucuresti</option>
                                <option value="ia">Iasi</option>
                                <option value="va">vaslui</option>
                            </select>
                            <div class="invalid-feedback">
                                Selectați Orașul
                            </div>
                        </div>
                    </div><!--end col-->

                    <!-- start Salary select block-->

                    <hr>


                    <div class="col-lg-12">
                        <div class="col-lg-6 mb-3">
                            <label for="Salariu_lunar">
                                Salariu lunar<span class="text-danger">*</span>
                            </label>
                        </div>
                        <div class="mb-4">
                            <div class="form-group chooseSalary mb-3 hasSalary salaryRange active">
                                <label class="form-label">
                                    <input type="radio"  class="form-check-input" name="salary_type" value="1" onclick="toggleDiv('salaryRange')" checked>
                                    Diapazon salarial
                                </label>

                                <div class="salaryTab form-inline align-items-center" id="salaryRange">
                                    <div class="form-group d-flex align-items-center">
                                        <div class="form-group d-flex align-items-center">
                                            <input type="text" class="form-control salaryInput m-lg-1" value="" name="salary_from" id="salary_from" placeholder="De la">
                                            <span class="text-secondary">—</span>
                                            <input type="text" class="form-control salaryInput m-lg-1" value="" name="salary_to" id="salary_to" placeholder="Până la">
                                        </div>

                                        <div class="col-2 m-lg-2">
                                            <select id="currency_range" class="form-select currency">
                                                <option value="MDL" selected="">MDL</option>
                                                <option value="USD">USD</option>
                                                <option value="EUR">EUR</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group chooseSalary mb-3 hasSalary salaryRange">
                                <label class="form-label">
                                    <input type="radio" class="form-check-input" name="salary_type" value="2" onclick="toggleDiv('salary_fix')">
                                    Salariu fix
                                </label>

                                <div class="salaryTab form-inline align-items-center" id="salary_fix" style="display: none;">
                                    <div class="form-group d-flex align-items-center">
                                        <div class="form-group d-flex align-items-center">
                                            <input type="text" class="form-control salaryInput m-lg-1" value="" name="fixed_salary" id="fixed_salary" placeholder="Summa">
                                        </div>

                                        <div class="col-2 m-lg-2">
                                            <select id="currency_fix" class="form-select currency">
                                                <option value="MDL" selected="">MDL</option>
                                                <option value="USD">USD</option>
                                                <option value="EUR">EUR</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group chooseSalary mb-3 hasSalary salaryRange">
                                <label class="form-label">
                                    <input type="radio" class="form-check-input" name="salary_type" value="3" onclick="toggleDiv('no_salary')">
                                    Nu indic
                                </label>
                                <div class="salaryTab form-inline align-items-center" id="no_salary" style="display: none;">
                                    <div class="form-group d-flex align-items-center">
                                        <div class="p-2.5 bg-yellow-100 mt-2.5" id="salaryAdvice">
                                            Indicați salariul pentru a primi mai multe candidaturi. În medie, CV-urile primite sunt de 3 ori mai multe în cazul anunțurilor de angajare cu salariu indicat.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
            <!-- end Salary select block-->
                    <div class="col-lg-12 mb-4">
                        <label class="form-label">
                            Categoria<span class="text-danger">*</span>
                        </label>
                        <div class="jspPane">
                            <ul class="sb_menu">
                                <li id="1" class="">
                                    <a href="javascript:m_sel(1, true)">IT, Programare</a>
                                </li>
                                <li id="47" class="">
                                    <a href="javascript:m_sel(47, true)">Achiziții</a>
                                </li>
                                <li id="31" class="">
                                    <a href="javascript:m_sel(31, true)">Asigurări</a>
                                </li>
                                <li id="43" class="active">
                                    <a href="javascript:m_sel(43, true)">Bănci, creditare</a>
                                </li>
                                <li id="34" class="">
                                    <a href="javascript:m_sel(34, true)">Comerț cu amănuntul</a>
                                </li>
                                <li id="32" class="">
                                    <a href="javascript:m_sel(32, true)">Construcții, Reparații, Arhitectură</a>
                                </li>
                                <li id="2" class="">
                                    <a href="javascript:m_sel(2, true)">Contabilitate, economiști</a>
                                </li>
                                <li id="10" class="">
                                    <a href="javascript:m_sel(10, true)">Cultură, artă</a>
                                </li>
                                <li id="46" class="">
                                    <a href="javascript:m_sel(46, true)">Cursuri, traininguri</a>
                                </li>
                                <li id="6" class="">
                                    <a href="javascript:m_sel(6, true)">Design</a>
                                </li>
                                <li id="7" class="">
                                    <a href="javascript:m_sel(7, true)">Entertainment, Show Business</a>
                                </li>
                                <li id="30" class="">
                                    <a href="javascript:m_sel(30, true)">Frumusețe, Fitness, Sport</a>
                                </li>
                                <li id="28" class="">
                                    <a href="javascript:m_sel(28, true)">Gospodării agricole</a>
                                </li>
                                <li id="15" class="">
                                    <a href="javascript:m_sel(15, true)">Imobile</a>
                                </li>
                                <li id="37" class="">
                                    <a href="javascript:m_sel(37, true)">Industria serviciilor</a>
                                </li>
                                <li id="8" class="">
                                    <a href="javascript:m_sel(8, true)">Ingineri</a>
                                </li>
                                <li id="41" class="">
                                    <a href="javascript:m_sel(41, true)">Instituții publice</a>
                                </li>
                                <li id="40" class="">
                                    <a href="javascript:m_sel(40, true)">Joburi fără experiență</a>
                                </li>
                                <li id="25" class="">
                                    <a href="javascript:m_sel(25, true)">Joburi pentru studenți, part-time</a>
                                </li>
                                <li id="39" class="">
                                    <a href="javascript:m_sel(39, true)">Juridică</a>
                                </li>
                                <li id="19" class="">
                                    <a href="javascript:m_sel(19, true)">Limbi străine, Traducători</a>
                                </li>
                                <li id="24" class="">
                                    <a href="javascript:m_sel(24, true)">Locuri de munca în străinătate</a>
                                </li>
                                <li id="50" class="">
                                    <a href="javascript:m_sel(50, true)">Lucru pentru pensionari</a>
                                </li>
                                <li id="49" class="">
                                    <a href="javascript:m_sel(49, true)">Lucru pentru persoane cu dizabilități</a>
                                </li>
                                <li id="9" class="">
                                    <a href="javascript:m_sel(9, true)">Magazinieri, Depozitari, Antrepozit</a>
                                </li>
                                <li id="14" class="">
                                    <a href="javascript:m_sel(14, true)">Manageri</a>
                                </li>
                                <li id="52" class="">
                                    <a href="javascript:m_sel(52, true)">Marketing de rețea</a>
                                </li>
                                <li id="12" class="">
                                    <a href="javascript:m_sel(12, true)">Marketing, publicitate, PR</a>
                                </li>
                                <li id="29" class="">
                                    <a href="javascript:m_sel(29, true)">Mass-media, jurnalism</a>
                                </li>
                                <li id="13" class="">
                                    <a href="javascript:m_sel(13, true)">Medicină, farmacie</a>
                                </li>
                                <li id="36" class="">
                                    <a href="javascript:m_sel(36, true)">Muncă la distanță de acasă</a>
                                </li>
                                <li id="48" class="">
                                    <a href="javascript:m_sel(48, true)">Muncă sezonieră</a>
                                </li>
                                <li id="26" class="">
                                    <a href="javascript:m_sel(26, true)">Muncitori auxiliari, hamali</a>
                                </li>
                                <li id="45" class="">
                                    <a href="javascript:m_sel(45, true)">Operatori telefonici, Call centre</a>
                                </li>
                                <li id="20" class="">
                                    <a href="javascript:m_sel(20, true)">Pedagogi, Traininguri</a>
                                </li>
                                <li id="18" class="">
                                    <a href="javascript:m_sel(18, true)">Personal birou</a>
                                </li>
                                <li id="3" class="">
                                    <a href="javascript:m_sel(3, true)">Personal casnic</a>
                                </li>
                                <li id="16" class="">
                                    <a href="javascript:m_sel(16, true)">Personal de serviciu</a>
                                </li>
                                <li id="21" class="">
                                    <a href="javascript:m_sel(21, true)">Producție</a>
                                </li>
                                <li id="22" class="">
                                    <a href="javascript:m_sel(22, true)">Psihologia</a>
                                </li>
                                <li id="5" class="">
                                    <a href="javascript:m_sel(5, true)">Restaurante, Alimentație Publică</a>
                                </li>
                                <li id="11" class="">
                                    <a href="javascript:m_sel(11, true)">Resurse Umane, HR, recrutare</a>
                                </li>
                                <li id="33" class="">
                                    <a href="javascript:m_sel(33, true)">Rețea de telecomunicații</a>
                                </li>
                                <li id="17" class="">
                                    <a href="javascript:m_sel(17, true)">Servicii pază, securitate</a>
                                </li>
                                <li id="4" class="">
                                    <a href="javascript:m_sel(4, true)">Șoferi</a>
                                </li>
                                <li id="42" class="">
                                    <a href="javascript:m_sel(42, true)">Stagiu, Internship</a>
                                </li>
                                <li id="23" class="">
                                    <a href="javascript:m_sel(23, true)">Top management</a>
                                </li>
                                <li id="35" class="">
                                    <a href="javascript:m_sel(35, true)">Transport, Logistică</a>
                                </li>
                                <li id="27" class="">
                                    <a href="javascript:m_sel(27, true)">Turism, Industria Ospitalității</a>
                                </li>
                                <li id="44" class="">
                                    <a href="javascript:m_sel(44, true)">Vânzări</a>
                                </li>
                                <li id="51" class="">
                                    <a href="javascript:m_sel(51, true)">Voluntariat</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <hr>
                    <div class="col-lg-6">
                        <div class="mb-4">
                            <label for="choices-single-cities" class="form-label">Program de muncă<span class="text-danger">*</span></label>
                            <select class="form-select cities" data-trigger="" name="choices-single-cities" id="choices-single-cities" aria-label="program de lucru" required>
                                <option selected disabled value="full-time">Full-time</option>
                                <option value="part-time">Part-time</option>
                                <option value="ture">În ture</option>
                                <option value="inegal">Inegal</option>
                                <option value="flexibil">Flexibil</option>
                            </select>
                            <div class="invalid-feedback">
                                Selectați programul de muncă
                            </div>
                        </div>
                    </div><!--end col-->

                    <div class="col-lg-6">
                        <div class="mb-4">
                            <label for="choices-single-cities" class="form-label">Studii</label>
                            <select class="form-select cities" data-trigger="" name="choices-single-cities" id="choices-single-cities" aria-label="studii" >
                                <option selected disabled value="full-time">Orice</option>
                                <option value="12">12 clase</option>
                                <option value="9">9 clase</option>
                                <option value="sp">Școală profesională</option>
                                <option value="colegiu">Colegiu</option>
                                <option value="universitate">Universitate</option>
                                <option value="cursuri">Cursuri</option>
                            </select>
                        </div>
                    </div><!--end col-->

                    <div class="col-lg-6">
                        <div class="mb-4">
                            <label for="choices-single-cities" class="form-label">Experiența de munca</label>
                            <select class="form-select cities" data-trigger="" name="choices-single-cities" id="choices-single-cities" aria-label="studii" >
                                <option selected disabled value="full-time">Orice</option>
                                <option value="fara-experienta">Posibil fără experiență</option>
                                <option value="1">De la 1 ani</option>
                                <option value="2">De la 2 ani</option>
                                <option value="3">De la 3 ani</option>
                                <option value="4">De la 4 ani</option>
                                <option value="5">De la 5 ani</option>
                            </select>
                        </div>
                    </div><!--end col-->
                    <div class="col-lg-12">
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="disability">
                            <label class="form-label" for="disability">Angajăm și persoane cu dizabilități</label>
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="refugiati">
                            <label class="form-label" for="refugiati">Dispuși să angajăm refugiați</label>
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="remote">
                            <label class="form-label" for="remote">Se poate lucra de acasa</label>
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="online_interview">
                            <label class="form-label" for="online_interview">Interviu online</label>
                        </div>
                    </div>

                    <hr>


                    <!-- Start Contact block -->

                    <div class="col-lg-12">
                        <div class="bg-soft-primary p-3 mb-5">
                            <h5 class="text-uppercase mb-0 fs-17">Informația de contact</h5>
                        </div>
                    </div>
                    <div class="col-lg-11">
                        <div class="form-group row mb-4">
                            <label for="contactPerson" class="col-sm-3 col-form-label">Persoana de contact</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="contactPerson">
                            </div>
                        </div>


                        <div class="form-group row mb-4">
                            <label for="inputPassword" class="col-sm-3 col-form-label">Telefon mob.</label>
                            <div class="col-sm-3">
                                <select type="text" class="form-control" id="mobPhone">
                                    <option value="1">Introduceți codul</option>
                                    <option value="1">505</option>
                                    <option value="1">505</option>
                                </select>
                            </div>
                            <div class="col-sm-5">
                                <input type="text" class="form-control mb-1" id="mobPhone" value="Introduceți codul" disabled>
                                <div class="mt-2">
                                    <a class="text-danger-emphasis " href="#">Adaugă înca un numar mobil <span class="text-danger"><i class="fas fa-plus"></i></span></a>
                                </div>
                            </div>

                        </div>

                        <div class="form-group row mb-4">
                            <label for="inputPassword" class="col-sm-3 col-form-label">Telefon/fax de oficiu</label>
                            <div class="col-sm-3">
                                <select type="text" class="form-control" id="mobPhone">
                                    <option value="1">Introduceți codul</option>
                                    <option value="1">405</option>
                                    <option value="1">705</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="officePhone" value="Introduceți codul" disabled>
                                <div class="mt-2">
                                    <a class="text-danger-emphasis" href="#">Adaugă înca un numar fix <span class="text-danger"><i class="fas fa-plus"></i></span></a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-4">
                            <label for="inputPassword" class="col-sm-3 col-form-label">E-mail de baza</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="baseEmail">
                                <div class="mt-2">
                                    <a class="text-danger-emphasis" href="#">Adaugă înca odresă email <span class="text-danger"><i class="fas fa-plus"></i></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-4">
                            <label for="inputPassword" class="col-sm-3 col-form-label">site</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="site">
                            </div>
                        </div>

                    </div><!--end col-->
                    <!--End Contact block -->



                    <div class="col-lg-12 ">
                        <div class="text-end">
                            <a href="javascript:void(0)" class="btn btn-success">Back</a>
                            <button class="btn btn-primary" type="submit">Post Now <i class="mdi mdi-send"></i></button>
                        </div>
                    </div><!--end col-->
                </div><!--end row-->
            </div><!--end job-post-content-->
        </form>
    </div><!--end container-->

    <input class="form-control form-control-sm form-control-solid" placeholder="Enter tags" value="tag1, tag2, tag3" id="kt_tagify_3"/>
    <input class="form-control form-control-solid" placeholder="Enter tags" value="tag1, tag2, tag3" id="kt_tagify_4"/>
    <input class="form-control form-control-lg form-control-solid" placeholder="Enter tags" value="tag1, tag2, tag3" id="kt_tagify_5"/>

</section>


<script>


    (() => {
        'use strict'
        const forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.from(forms).forEach(form => {
            form.addEventListener('submit', event => {
                if (!form.checkValidity()) {
                    event.preventDefault()
                    event.stopPropagation()
                }

                form.classList.add('was-validated')
            }, false)
        })
    })()


    var lastSelectedDivId = "salaryRange"; // ID div, который отображается по умолчанию

    function toggleDiv(divId) {
        var div = document.getElementById(divId);

        // Check if there was a previously selected div
        if (lastSelectedDivId !== null) {
            var lastSelectedDiv = document.getElementById(lastSelectedDivId);
            lastSelectedDiv.style.display = "none"; // Hide the previously selected div
        }

        // Update the last selected div id
        lastSelectedDivId = divId;

        // Toggle the display of the current div
        if (div.style.display === "none") {
            div.style.display = "block";
        } else {
            div.style.display = "none";
        }
    }

</script>
@endsection
