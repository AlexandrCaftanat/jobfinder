@extends('layouts.main')
@section('content')
<!-- Begin page -->


<!-- START SIGN-IN -->

<section class="page-title-box .bg-soft-purple">
    <div class="container">
        <div class="row justify-content-center mb-5">
            <div class="col-lg-6 ">
                <div class="section-title text-center text-white">
                    <h1 class="title">Înregistrează un cont nou</h1>
                    <p class="fs-18">Alege tipul de cont potrivit pentru tine</p>
                </div>
            </div><!--end col-->
        </div><!--end row-->
        <div class="row">
            <div class="row justify-content-center">

                <div class="row">

                    <div class="col-sm-6 mb-4">
                        <div class="card service-box">
                            <div class="card-body worker">
                                <div class="row">
                                    <span class="text-end cl"><i class="fas fa-info-circle" data-bs-toggle="tooltip" data-bs-placement="left" title="Dacă ești în căutare de angajați, alege acest tip de cont."></i></span>
                                    <h3 class="card-title add-acc text-center">Companie</h3>
                                </div>
                                <div class="list-green-check mb-4">
                                    <ul class="list-green-check mb-3">
                                        <li>Promovare personalizată a anunțurilor de angajare;</li>
                                        <li>Promovarea brandului de angajator;</li>
                                        <li>Publicare nelimitată de oferte de muncă;</li>
                                        <li>Suport 24/7</li>
                                    </ul>
                                </div>
                                <div class="text-center">
                                    <a href="{{route('job.add')}}" class="btn btn-danger2 btn-padding">Adaugă Job</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <div class="card employer service-box">
                            <div class="card-body">
                                <div class="row">
                                    <span class="text-end cl"><i class="fas fa-info-circle" data-bs-toggle="tooltip" data-bs-placement="left" title="Dacă ești în căutarea unui loc de muncă, alege acest tip de cont."></i></span>
                                    <h3 class="card-title add-acc text-center">Candidat</h3>
                                </div>
                                <div class="list-green-check mb-4">
                                    <ul class="list-green-check mb-3">
                                        <li>8400 locuri de muncă;</li>
                                        <li>2000 companii angajatoare;</li>
                                        <li>Posibilitatea de a-ți crea un CV profesionist;</li>
                                        <li>Informații actuale despre piața muncii.</li>
                                    </ul>
                                </div>
                                <div class="text-center">
                                    <a href="#" class="btn btn-danger2 btn-padding">Adaugă CV</a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div><!--end row-->
    </div><!--end container-->
</section>
<!-- END SIGN-IN -->
@endsection
