<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Main\IndexController;


Route::get('/sig',  function (){
    return view('main.sign-in');
});
Route::get('/det',  function (){
    return view('main.job-details');
});

Route::get('/ccc',  function (){
    return view('main.company-details');
});

Route::get('/jp',  function (){
    return view('main.company_dash_board');
})->name('job.add');

Route::get('/mjp',  function (){
    return view('main.manage-jobs-post');
});

Route::group(['namespace' => 'App\Http\Controllers\Main'], function () {
Route::get('/', IndexController::class);
});

Auth::routes();

